#include "header.h"
#include <string.h>
#include <time.h>
#include <ctype.h>

#define OFFSET  22
#define MAXDATE 25

static const char *query;
static char path[40];
static char *root;
static char *sroot;
static int current_day;

static void
die(const char *s)
{
	printf(TEXTHEAD);
	printf("%s\n", s);
	exit(0);
}

static void
get_time(void)
{
	time_t t = time(NULL);
	if (!t)
		die("Could not get time!\n");
	struct tm *tmp = gmtime(&t);
	current_day = tmp->tm_mday;
}

static int
h_img(void)
{
	char *s, *t; int n;

	s = strstr(query, "img");
	if (!s)
		return -1;

	s += 4; /* Skip over the "img=" part */

	for (t = s; *t; t++)
		if (!isdigit(*t))
			die("Invalid input\n");
		

	return *s == '0' ? 0 : atoi(s);
}

static int
h_time(int n)
{
	if (OFFSET+n > current_day)
		return 0;
	return 1;
}

static const char *
booltostr(int b)
{
	if (b)
		return "Yes";
	return "No";
}

int
exists(int n)
{
	FILE *file;
	if (h_time(n)) {
		sprintf(path, "%shostfile/%d.png", root, n);
	} else {
		sprintf(path, "%shostfile/a%d.png", root, n);
	}
		
	if ((file = fopen(path, "r"))) {
		fclose(file);
		if (h_time(n)) {
			sprintf(path, "%shostfile/%d.png", sroot, n);
		} else {
			sprintf(path, "%shostfile/a%d.png", sroot, n);
		}
		return 1;
	}
	return 0;
}

int
main(void)
{
	query = getenv("QUERY_STRING");
	sroot = "http://finnoleary.net/";
	root = getenv("DOCUMENT_ROOT");
	get_time();

	int n = h_img();
	if (n+OFFSET > MAXDATE)
		die("Image not availible!\n");

	int e = exists(n);
	if (e) {
		printf(TEXTHEAD);
		printf("Location: %s\n", path);
		/* die(path);
		printf(IMGHEAD);
		FILE *in = fopen(path, "r");
		int c;
		while ((c = getc(in)) && !ferror(in) && !feof(in)) {
			fputc(c, stdout);
		} */
	} else {
		die("Image doesn't exist on server!\n");
	}


	/* printf("Image Number: %d\n", n);
	printf("Day of image: %d\n", n+OFFSET);
	printf("Current day: %d\n", current_day);
	printf("Can show image? %s\n", booltostr(h_time(n))); */
	
	return 0;
}
