%verbatim <!DOCTYPE html>

(html
  %include 'src/incs/head.w'
  (body
    (h2 'Finn O\'Leary')
    (p "I'm a programmer living in the misty mountains of Wales")
    (p )
    (p "I have a strong passion for free software and support organizations
	such as the EFF and the GNU foundation. As well as having committed
	code to the Linux man-pages project, I have also been semi-active on
	the Alpine Linux mailing list, and I try to back efforts that make
	programs not just more-predictable and safer, but also smaller, such
	as 2f30 and the suckless project.")
    (p )
    (p "I am a strong proponent of functional programming and the UNIX
        methodology, and have wide-spanning interests, including a love of
	getting 'close to the metal'.")
    (p "I have recently been investing time in reverse engineering and writing
        Python clients for Android apps, and will be able to publish some of
	my work as soon as the responsible disclosure time period has
	completed.")
    (p "Some of my non-technical interests include playing musical instruments
        such as the banjo and violin, and reading up on philosophy and politics.")
    (p )
    (p "My three favourite writers are Terry Pratchett, Ursula LeGuin, and Stanisław Lem")
    (p )
    (span 'You can find my CV here:'
          (a :href './cv.txt'
	     'plain text'
	     %nonewline
	     %noindent)
	  '.'
	  (a :href './cv.1'
	     'man page'
	     %noindent
	     %nonewline)
	  '.'
	  (a :href './cv.pdf'
	     'PDF'
	     %noindent
	     %nonewline))
    (p )
    (h3 "Writing")
    (div (a :href './archivist-postmortem.html'
            "Archivist: A post-mortem")
	 (p )
         (a :href './kernel-code.html'
            "Dissecting Linux Kernel Code")
         (p )
         (a :href './macro-abuse.html'
            "Macro abuse or \"Where have my function arguments gone?\"")
         (p )
         (a :href './koios-tutorial.html'
            "Koios User Tutorial")
         (p ))
         ;(a :href './tools-i-like.html'
         ;   "Assorted Links That I Find Interesting"))
    (h3 "Works")
    (p (a :href 'https://gitlab.com/finnoleary/koios'
          "koios / libkoios  -  File tagging system utilizing extended attributes")
       (p )
       (a :href 'https://gitlab.com/finnoleary/zhmenu'
          "zhmenu  -  Adaptable pinyin-to-chinese input menu")
       (p )
       (a :href 'https://gitlab.com/finnoleary/spotifyc'
          "spotifyc  -  C library and CLI for the Spotify Web API")
       (p )
       (a :href 'https://gitlab.com/finnoleary/wisp-new'
          "wisp  -  HTML simplifier and macro system (used to generate this website)")
       (p )
       (p (a :href 'https://snark.badcode.rocks/archives/2018-October/000004.html'
             "Submission"
	     %nonewline
	     %noindent)
          "for the October 2018 Bad Code Contest (Winner - "
	  (a :href 'http://badcode.rocks/2018/333/october-teardown-pig-latin/'
	     "teardown here"
	     %nonewline
	     %noindent)
	  %nonewline
	  %noindent
	  ")")
       (a :href 'https://gitlab.com/finnoleary/twc'
          "twc  -  tiny word count (much faster than GNU wc)")
       (p )
       (a :href 'https://gitlab.com/finnoleary/one-offs/tree/master/replace'
          "replacer  -  file includer replacer (much faster than rpr)")
       (p )
       (a :href 'https://gitlab.com/finnoleary/lolsp'
          "LOLsp  -  a typed lisp where everything is made out of lists"))
    (h3 "Contact")
    (p (a :href 'mailto:finnoleary@inventati.org' 'finnoleary@inventati.org')
       (p )
       (a :href 'https://news.ycombinator.com/user?id=fao_' 'Hacker News account')
       (p )
       (a :href 'https://gitlab.com/finnoleary/' 'Gitlab account')
       (p )
       (a :href 'https://github.com/finnoleary' 'Github account (mostly unused)')
       (p )
       (a :href 'https://github.com/gallefray' 'Second Github account (pretty old)')
       (p )
       (a :href 'http://ludumdare.com/compo/author/gallefray/' 'Ludum Dare')
       (p ))))

