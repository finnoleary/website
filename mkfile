input= `{ ls src/*.w }
output=./public/

rename= `{ (command -v rename.ul) || (command -v rename) }

do:QV: $output
 :

$output:V: $input
	wisp -d $target $input
	cp src/style.css $target
	cp src/*.svg $target
	cp src/cv.* $target
	$rename .w.html .html public/*.w.html

clean:V:
  rm -rf $output
